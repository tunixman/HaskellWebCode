{-# LANGUAGE OverloadedStrings, TemplateHaskell #-}
module Main where

import           Control.Monad (liftM)
import           Control.Monad.State (get)
import           Control.Monad.IO.Class (liftIO)
import           Control.Applicative
import           Control.Exception (SomeException, try)
import           Control.Lens.TH
import           Snap
import           Snap.Snaplet.Config
import           System.IO
import qualified Data.Text as T
import           Paths_HaskellWeb (getDataDir)
import           Data.Configurator

data Message = Message {
    _message :: Snaplet T.Text
    }

makeLenses ''Message

data Site = Site {
    _hello :: Snaplet Message
    }

makeLenses ''Site

dataDir :: IO String
dataDir = liftM (++"/resources/SingleConfig") getDataDir

messageInit :: SnapletInit b T.Text
messageInit = makeSnaplet "message" "Message" ( Just dataDir) $
    addRoutes [("", messageHandler)] >>
    getSnapletUserConfig >>=
    \c -> liftIO $ lookupDefault "NO MSG" c "message"
          >>= return 

helloInit :: SnapletInit b Message
helloInit = makeSnaplet "hello" "Hello" (Just dataDir) $
            Message <$> nestSnaplet "message" message messageInit

messageHandler :: Handler b T.Text ()
messageHandler =
    get >>= writeText

site :: SnapletInit b Site
site = makeSnaplet "App" "Application" Nothing $ 
       Site <$> nestSnaplet "hello" hello helloInit

-- Main: snaplet runner
main :: IO ()
main =
    commandLineAppConfig defaultConfig
    >>= runSite

runSite :: Config Snap AppConfig -> IO ()
runSite conf =
    runSnaplet (appEnvironment =<< getOther conf) site
    >>= \(mm, si, cu) ->
            do hPutStrLn stderr $ T.unpack mm
    >> serve conf si >> cu

serve ::
    Config Snap a ->
    Snap () ->
    IO (Either SomeException ())
serve c s =
    try $ httpServe c s 
