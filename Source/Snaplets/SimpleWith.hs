{-# LANGUAGE OverloadedStrings, TemplateHaskell #-}
module Main where

import           Control.Monad (liftM)
import           Control.Monad.State (get)
import           Control.Monad.IO.Class (liftIO)
import           Control.Applicative
import           Control.Exception (SomeException, try)
import           Control.Lens.TH
import           Snap
import           Snap.Snaplet.Config
import           System.IO
import qualified Data.Text as T
import           Paths_HaskellWeb (getDataDir)
import           Data.Configurator

data Message = Message {
    _message :: T.Text
    }

makeLenses ''Message

data Site = Site {
    _response :: Snaplet Message
    }

makeLenses ''Site

dataDir :: IO String
dataDir = liftM (++"/resources/SingleConfig") getDataDir

messageInit :: SnapletInit b Message
messageInit = makeSnaplet "message" "Message" (Just dataDir) $
    getSnapletUserConfig >>=
    \c -> liftIO $ lookupDefault "NO MSG" c "message"
          >>= return . Message
                     
messageHandler :: Handler b Message ()
messageHandler =
    get >>= \(Message m) -> writeText m

site :: SnapletInit b Site
site = makeSnaplet "App" "Application" Nothing $
       addRoutes [("message", with response messageHandler)] >>
                 Site <$>
                      nestSnaplet "message" response messageInit

-- Main: snaplet runner
main :: IO ()
main =
    commandLineAppConfig defaultConfig
    >>= runSite

runSite :: Config Snap AppConfig -> IO ()
runSite conf =
    runSnaplet (appEnvironment =<< getOther conf) site
    >>= \(mm, si, cu) ->
            do hPutStrLn stderr $ T.unpack mm
    >> serve conf si >> cu

serve ::
    Config Snap a ->
    Snap () ->
    IO (Either SomeException ())
serve c s =
    try $ httpServe c s 
